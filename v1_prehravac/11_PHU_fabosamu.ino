#include <Esplora.h>
#include <SPI.h>
#include <SD.h>
#include <TFT.h> // Hardware-specific library

File myFile;
int arr [50];
int arrSize = 0;

void setup() {
  Serial.begin(9600);
  initScreen();
//  waitForSerial();
  if( ! initSD() ){
    screenBadSD();
    while(1);
  }
  writeToFileSerial();
//  readFileSerial();
  readFileToArray();
  printArray();
  display_menu(true);
}

void loop() {
  Menu();
}
