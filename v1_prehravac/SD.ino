bool initSD()
{
 Serial.print("Initializing SD card...");

  if (!SD.begin(8)) {
    Serial.println("initialization failed!");
    return false;
  }
  Serial.println("initialization done."); 
  return true;
}

void waitForSerial()
{
   showOpenSerial(true);
   while (! Serial ){
    } 
   showOpenSerial(false);
   Serial.println("Welcome!");
}

void writeToFileSerial()
{
  //  handle prevoius file
  if (SD.exists("sheet.txt")) {
     SD.remove("sheet.txt");
  }
  myFile = SD.open("sheet.txt", FILE_WRITE);
  if (myFile) {
    Serial.print("Writing to sheet.txt...");
    
    myFile.println("26 24 26 21 17 20 14 0");
    myFile.println("26 24 26 21 17 20 14 0");
    myFile.println("26 28 29 28 29 26 28 26 28 24 26 24 26 22 26 0");
    myFile.close();
    Serial.println("done!");
  } else {
    Serial.println("error opening sheet.txt");
  }
}

void readFileSerial ()
{
  myFile = SD.open("sheet.txt");
  if (myFile) {
    Serial.println("sheet.txt:");
    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      Serial.write(myFile.read());
    }
    myFile.close();
  } else {
    Serial.println("error opening sheet.txt");
  }
}

void readFileToArray ()
{
  myFile = SD.open("sheet.txt");
  if (myFile) {
    Serial.println("sheet.txt: readtoarray");
    
    arrSize = 0;
    
    while (myFile.available()) {
      arr[arrSize] = myFile.parseInt();
      arrSize++;
    }
    // close the file:
    myFile.close();
    Serial.println("readArray done");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening sheet.txt");
  }
}

void printArray()
{
  Serial.println("printing array");
  for (int i = 0 ; i < arrSize ; i++ ){
   Serial.println(arr[i]); 
  }
}
