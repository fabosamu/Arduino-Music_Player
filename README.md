# Projektová dokumentácia

## Názov projektu:**Prehrávač "hudby" a tvorba hudby**

- Autor:*Samuel Fabo*

### Základný cieľ:

Cieľom práce bolo simulovať dávne časy tlačidlových telefónov a ich MIDI
zvonenia.

### Realizácia:

Pri realizácii som používal prípravok Arduino Esplora, ktorý mi bol
vypožičaný od FIT, ČVUT.

Na tomto prípravku je realizovaná celá funkcionalita aj uživateľské
rozhranie.

Využívam pri tom čítačku SD karty, TFT display, tlačidlá na základnej
doske, ako aj tlačidlo JoySticku. Celá neskoršia funkcionalita sa odvíja
od pieza integrovaného na Esplore.

Využívam taktiež logiky a aritmetiky samotných hudobných frekvencií a
nôt, použitá bola funkcia na ich výpočet zo stránky
<http://www.phy.mtu.edu/~suits/NoteFreqCalcs.html>

Poznámka: pri realizácii celého projektu som narazil na menší problém, a
síce pamäť samotného prípravku. Preto som bol nútený rozdeliť túto
semestrálnu prácu na dva funkčné programy.

### Uživateľské rozhranie

1\. časť -- prehrávanie hudby z SD karty

Už pri nahraní a spustení celého programu sa zobrazí uvítacia obrazovka.

V prípade nesprávne zvolenej SD karty alebo karty, z ktorej sa nedá
načítať obsah sa zobrazí hlásenie "Bad SD card!"

Ako základ celej práce som implementoval menu s výberom funkcie:

**Play from SD**

**Play reverse**

Pri zvolení prvej položky sa prehrá obsah súboru sheet.txt nahraný na SD
karte (pozn. Nahrávanie nôt do súboru je realizované pri spustení
programu)

Pri zvolení druhej položky sa obsah prehrá odzadu.

2\. časť -- vytváranie vlastnej melódie

pozn. Tento program nevyužíva knižnice SD karty

Po načítaní úvodnej stránky sa zobrazí menu:

**Play demo**

**Make your own melody**

Pri zvolení **prvej** položky sa prehá obsah napevno nahratej sekvencie
nôt

Pri zvolení **druhej** položky sa spustí uživateľské rozhranie pre
tvorbu nôt:

Uživateľ mení výšku noty tlačidlami HORE a DOLE, volí ju tlačidlom
VPRAVO.

Pokiaľ chce zadanú melódiu prehrať, stlačí TLAČIDLO JOYSTICKU (push).

Pre návrat do menu stačí stisknúť tlačidlo VĽAVO

### Záver

Na záver by som chcel poďakovať cvičiacim predmetu BI-ARD za spoluprácu
a množstvo rád pri tvorbe tejto semestrálnej práce.
