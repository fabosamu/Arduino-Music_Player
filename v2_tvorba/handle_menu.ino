#define DOWN  SWITCH_1
#define UP    SWITCH_3
#define ENTER SWITCH_4
#define BACK  SWITCH_2
#define CS   10
#define DC   9
#define RESET  8

byte buttonFlag = 0;

enum states {
  HOME_SEL_SD, HOME_SEL_OWN, SD_sel, OWN_sel
};
enum states STATE, NEXT_STATE;

 bool buttonEvent(int button)
{
  switch(button)
  {
    case UP:
     if (Esplora.readButton(UP) == LOW)
     {
       buttonFlag |= 1;
     }
     else if (buttonFlag & 1)
     {
       buttonFlag ^= 1;
       return true;
     }
     break;
 
    case DOWN:
     if (Esplora.readButton(DOWN) == LOW)
     {
       buttonFlag |= 2;
     }
     else if (buttonFlag & 2)
     {
       buttonFlag ^= 2;
       return true;
     }
     break;
 
    case BACK:
     if (Esplora.readButton(BACK) == LOW)
     {
       buttonFlag |= 4;
     }
     else if (buttonFlag & 4)
     {
       buttonFlag ^= 4;
       return true;
     }
     break;
 
    case ENTER:
     if (Esplora.readButton(ENTER) == LOW)
     {
       buttonFlag |= 8;
     }
     else if (buttonFlag & 8)
     {
       buttonFlag ^= 8;
       return true;
     }
  }
   return false;
}


 unsigned long time;
 void myDelay(unsigned int amount){
   time = millis();
   while( millis() <= time + amount ){
     if(buttonEvent(BACK))
          {
            display_menu(true);
            NEXT_STATE = HOME_SEL_SD;
            break;
          }
   }
 }


char x = 10;
char y = 10;
int count = 0;
int a [31];
int aSize = 0;

void Menu()
{
  switch (STATE)
   {
      case HOME_SEL_SD:
          if (buttonEvent(DOWN))
          {
            change_position(30,60);
            NEXT_STATE = HOME_SEL_OWN;
          }
          else if (buttonEvent(ENTER))
          {
            display_menu(false);
            NEXT_STATE = SD_sel;
          }
          break;
 
      case HOME_SEL_OWN:
          if (buttonEvent(UP))
          {
            change_position(60,30);
            NEXT_STATE = HOME_SEL_SD;
          }
          else if (buttonEvent(ENTER))
          {
            display_menu(false);
            NEXT_STATE = OWN_sel;
            count = 0;
            showNote(count, x, y,false);
          }
          break;

      case SD_sel:
          if(buttonEvent(BACK))
          {
            display_menu(true);
            NEXT_STATE = HOME_SEL_SD;
          }
          else
          {
            showSD();
            playDemo();
            //display_menu(true);
            NEXT_STATE = HOME_SEL_SD;
          }
          break;
 
      case OWN_sel:
          
          if(buttonEvent(BACK))
          {
            x = 10;
            y = 10;
            count = 0;
            for ( int i = 0 ; i <= aSize ; i++ ){
              a[i]=0;
            }
            aSize = 0;
            display_menu(true);
            NEXT_STATE = HOME_SEL_SD;
          }
          else if (buttonEvent(UP))
          {
            count++;
            Serial.println(count);
            showNote(count, x, y,true);
          } 
          else if (buttonEvent(DOWN))
          {
            if ( count > 0 )
              count--;
            Serial.println(count);
            showNote(count, x, y,false);
            
          } 
          else if (buttonEvent(ENTER)) 
          {
            if ( aSize <= 30 ){
              a[aSize] = count;
              aSize ++;
              count = 0;
              x += 20;
              if ( x >= 110 ){
                x = 10;
                y += 20;
              }
              showNote(count, x, y,false);
            }
          }
          else if (Esplora.readJoystickSwitch() == 0)
          {
            playOwn();
          }
          else
          {
//            showNote(count, x, y,true);
          }
          break;
   }
   STATE = NEXT_STATE;
}
