void playNote (int freq, /*int dur,*/ int del){
  Esplora.tone( freq );
  delay(150);
  Esplora.noTone();
  myDelay(del);
}

int countNote(int n){
  double value;
  if( n != 0 ){
    value = 110 * pow(1.059463094359, n);
  } else {
    value = 0;
  }
  return (int) value;
}

void playDemo()
{
  for ( int i = 0 ; i < arrSize ; i++ ){
    playNote( countNote(arr[i]), 50);
    if( NEXT_STATE == HOME_SEL_SD ) break;
  }
}

void playOwn()
{
  for ( int i = 0 ; i <= aSize ; i++ ){
    playNote(countNote(a[i]), 50);
  }
}
