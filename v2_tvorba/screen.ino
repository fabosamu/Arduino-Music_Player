void initScreen ()
{
    EsploraTFT.begin(); 
    EsploraTFT.setTextSize(1);
    EsploraTFT.background(0,0,0);
    EsploraTFT.stroke(255,255,255);
    EsploraTFT.text("Welcome!",5,10);
    EsploraTFT.setTextSize(2);
    EsploraTFT.text("Play songs,",10,40);
    EsploraTFT.text("Make songs",10,60);
    EsploraTFT.setTextSize(1);
    EsploraTFT.text(".Samuel Fabo",80,110);
    //delay(500);
    STATE = HOME_SEL_SD;
}

void screenBadSD ()
{
    EsploraTFT.setTextSize(2);
    EsploraTFT.background(0,0,0);
    EsploraTFT.stroke(255,255,255);
    EsploraTFT.text("BAD SD CARD!",10,40);
    delay(300);
    EsploraTFT.setTextSize(1);
}

 void display_menu(bool renew){
  if (renew == false){
    EsploraTFT.stroke(0,0,0);
    EsploraTFT.text(">",20,60);
  } else {
    EsploraTFT.background(0,0,0);
    EsploraTFT.stroke(255,255,255);
  }
  EsploraTFT.text("Select:",0,0);
  EsploraTFT.text(">",20,30);
  EsploraTFT.text(" Play demo",30,30);
  EsploraTFT.text(" Make my own melody",30,60); 
 }

 void change_position(int posB, int posE){
   EsploraTFT.stroke(0,0,0);
   EsploraTFT.text(">",20,posB);
   EsploraTFT.stroke(255,255,255);
   EsploraTFT.text(">",20,posE);
 }

void showOpenSerial(bool er)
{
  if (er){
   EsploraTFT.stroke(255,255,255);
   EsploraTFT.text("Open Serial!",10,20);
  } else {
    EsploraTFT.stroke(0,0,0);
   EsploraTFT.text("Open Serial!",10,20);
  }
}

void showSD()
{
  EsploraTFT.stroke(255,255,255);
  EsploraTFT.text("This is demo song",10,40);
  EsploraTFT.text("Popcorn, by G. Kingsley",5,60);
  myDelay(1000);
}

char notes [12][4]= {"2A ","2A#","2H ","2C ","2C#","2D ","2D#","2E ","2F ","2F#","2G ","2G#"};
char prev [4] = {"---"};
void showNote(int s, char x, char y, bool up)
{
  int sel = s % 12;
  
  int nr = 50;
  
  notes[sel][0] = nr;
  EsploraTFT.stroke(0,0,0);
  EsploraTFT.text( prev, x, y );
  
  if ( s == 0 ){
    EsploraTFT.stroke(255,255,255);
    EsploraTFT.text( "--", x, y );
  } else {
    EsploraTFT.stroke(255,255,255);
    EsploraTFT.text( notes[sel], x, y );
  }
  
  strncpy(prev,notes[sel],4);
}
